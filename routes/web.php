<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Product;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/car', function () {
    return view('car.index');
});

Route::get('/products', 'ProductController@index');
Route::get('/product/list/{last}/{limit}', function ($last, $limit) {
    $products = \App\Product::offset($last)->limit($limit)->get();
    echo $products->toJson();
});
Route::get('/product/{id}', 'ProductController@get');