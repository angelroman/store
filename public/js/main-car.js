$(document).ready(function () {
    var car = new Car(),
        $myFooter = $(".myFooter");
    localStorage.Car = localStorage.Car || "{ \"Products\": [], \"Items\": [] }";
    car.set(JSON.parse(localStorage.Car));


    if(car.Items.length > 0) {
        car.Items.map(function (item) {
            $(".order-products-list").append(item.getTemplate());
            item.AddEventChangeQuantity(function () {
                car.setStorageCar();
                car.showPurchaseSummary();
            });
            item.AddEventRemove(function () {
                if(car.removeItem(item.product_id)) {
                    car.setStorageCar();
                    if(car.Items.length == 0) {
                        $(".order-products-list").append(car.getTemplateEmptyCar());
                    }
                    car.showPurchaseSummary();
                }
            });
        });
    }
    else {
        $(".order-products-list").append(car.getTemplateEmptyCar());
    }
    car.showPurchaseSummary();
    console.log(car);
    $myFooter.fadeIn('slow');
});