var spinner;

$(document).ready(function () {
    spinner = new Spinner();
    $('.scrollspy').scrollSpy();
});


function Spinner() {
    this.id = Math.floor(Math.random() * (9999 - 1)) + 1;
    this.spinner = "<div class='containerSpinner' id='spinner"+this.id+"'><div class='preloader-wrapper big active'><div class='spinner-layer spinner-blue-only'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div> <h4 class='textLoad'>Cargando...</h4></div>";

    this.show = function () {
        $('body').append(this.spinner);
    };
    this.remove = function() {
        $("#spinner"+this.id).remove();
    };
}