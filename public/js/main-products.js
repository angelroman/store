$(document).ready(function () {
    var $divListProducts = $("#divListProducts"),
        $btnCarrito = $("#btnCarrito"),
        $loadMore = $(".load-more"),
        $myFooter = $(".myFooter"),
        car = new Car(),
        list = new ILista(new Product()),
        last = 0;

    localStorage.Car = localStorage.Car || "{ \"Products\": [], \"Items\": [] }";
    car.set(JSON.parse(localStorage.Car));

    list.getList(last, 12, function () {
        spinner.show();
    }, function (response) {
        last += 12;
        var _product = null;
        var templates = "";
        response.map(function (product) {
            _product = new Product();
            Object.assign(_product, product);
            templates += _product.getTemplateProduct();
        });
        $divListProducts.append(templates);
        $divListProducts.fadeIn('slow');
        $(".agregarCarrito").click(function () {
            var id = this.id.substr(10);
            car.addItem({ id: id });
        });
    }, function () {}, function () {
        console.log(car);
        spinner.remove();
        car.showBadgesQuantityProducts();
        car.showItems();
        $loadMore.fadeIn('slow');
        $myFooter.fadeIn('slow');

    });
    $btnCarrito.click(function () {
        /*car.showOrders();*/
        /*car.showItems();*/
        $("#carrito").modal("open");
    });
    $loadMore.click(function () {
        list.getList(last, 12, function () {
            $loadMore.fadeOut('fast');
            spinner.show();
        }, function (response) {
            console.log(response);
            last += 12;
            var _product = null;
            var templates = "";
            response.map(function (product) {
                _product = new Product();
                Object.assign(_product, product);
                templates += _product.getTemplateProduct();
            });
            $divListProducts.append(templates);
            $(".agregarCarrito").unbind();
            $(".agregarCarrito").click(function () {
                var id = this.id.substr(10);
                car.addItem({ id: id });
            });
        }, function () {}, function () {
            spinner.remove();
            car.showBadgesQuantityProducts();
            $loadMore.fadeIn('slow');
        });
    });
});