function Car() {
    this.Products = [];
    this.Items = [];

    this.purchaseSummary = {
        subtotal: 0,
        tax: 0,
        total: 0,
        shippingCost: 0
    };

    this.set = function (car) {
        var _product = null, _item = null, self = this;
        car.Products.map(function (product) {
            _product = new Product();
            Object.assign(_product, product);
            self.Products.push(_product);
        });
        car.Items.map(function (item) {
           _item = new Item();
            Object.assign(_item, item);
            _item.setProduct(item.Product);
            self.Items.push(_item);
        });
    };
    this.setStorageCar = function () {
        localStorage.Car = JSON.stringify(this);
    };

    this.addItem = function (product) {
        var position = this.findPositionItem(product),
            self = this;
        if(position != -1) {
            self.Items[position].quantity++;
            self.updateBadgeQuantity(product.id, self.Items[position].quantity);
            self.Items[position].updateTemplateQuantity();
            self.setStorageCar();
        }
        else {
            var item = new Item();
            item.Product = new Product();
            item.Product.id = product.id;
            item.product_id = product.id;
            item.Product.get(function () { spinner.show(); }, function (response) {
                Object.assign(item.Product, response);
                item.quantity = 1;
                self.Items.push(item);
                self.updateBadgeQuantity(item.product_id, 1);
                self.addItemToSummaryCar(item);
                self.setStorageCar();
                Materialize.toast("Producto agregado", 1000);
            },function () {}, function () { spinner.remove(); });
        }
    };
    this.addItemToSummaryCar = function (item) {
        var self = this;
        $(".order-products-list").append(item.getTemplateModal());
        item.AddEventChangeQuantity(function () {
            self.updateBadgeQuantity(item.product_id, item.quantity);
            self.setStorageCar();
        });
        item.AddEventRemove(function () {
            if(self.removeItem(item.product_id)) {
                $("#quantityOrder"+item.product_id).fadeOut("fast").empty();
            }
        });
    };
    this.showBadgesQuantityProducts = function () {
        var self = this;
        this.Items.map(function (item) {
            self.updateBadgeQuantity(item.product_id, item.quantity);
        });
    };
    this.updateBadgeQuantity = function (product_id, quantity) {
        $("#quantityOrder"+product_id).text(quantity);
        $("#quantityOrder"+product_id).css('display', 'inline');
    };
    this.findPositionItem = function (product) {
        for(var i = 0; i < this.Items.length; i++) {
            if(this.Items[i].product_id == product.id) return i;
        }
        return -1;
    };

    //Muestra todos los items en el modal footer
    this.showItems = function () {
        var self = this;
        self.Items.map(function (item) {
            self.addItemToSummaryCar(item);
        });
    };
    this.removeItem = function (product_id) {
        var position = this.findPositionItem({ id: product_id });
        if(position != -1) {
            this.Items.splice(position, 1);
            this.setStorageCar();
            return true;
        }
        return false;
    };
    this.getTemplateEmptyCar = function () {
        return "<div class='row'><div class='col s12'><div class='card-panel red accent-2 white-text'>Su carrito est&aacute; vacio</div></div></div>";
    };

    this.showPurchaseSummary = function () {
        this.getPurchaseSummary();
        $("#tdSubtotal").text("$ " + this.purchaseSummary.subtotal);
        $("#tdEnvio").text("$ " + this.purchaseSummary.shippingCost);
        $("#tdTotal").text("$ " + this.purchaseSummary.total);
    };
    this.getPurchaseSummary = function () {
        var self = this;
        self.resetPurchaseSummary();
        self.Items.map(function (item) {
            self.purchaseSummary.subtotal += parseFloat(item.getTotal());
        });
        self.purchaseSummary.total
            = parseFloat(self.purchaseSummary.subtotal)
            + parseFloat(self.purchaseSummary.tax)
            + parseFloat(self.purchaseSummary.shippingCost);
        self.purchaseSummary.subtotal = self.purchaseSummary.subtotal.toFixed(2);
        self.purchaseSummary.shippingCost = self.purchaseSummary.shippingCost.toFixed(2);
        self.purchaseSummary.total = self.purchaseSummary.total.toFixed(2);
    };
    this.resetPurchaseSummary = function () {
        this.purchaseSummary.subtotal = 0;
        this.purchaseSummary.tax = 0;
        this.purchaseSummary.total = 0;
        this.purchaseSummary.shippingCost = 0;
    }

}