function Product() {
	this.id = null;
	this.name = null;
	this.code = null;
	this.price = null;
	this.stok = null;

	this.Category = {};
	this.Unit = {};
	this.Brand = {};
	this.description = null;
    this.Images = [];
    this.path = "../product/";

    this.get = function (fnBeforeSend, fnSuccess, fnError, fnComplete) {
        var self = this;
        $.ajax({
            url: self.path + self.id,
            type: "GET",
            dataType: "JSON",
            beforeSend: function () { if(fnBeforeSend != undefined) fnBeforeSend(); },
            success: function (response) { if(fnBeforeSend != undefined) fnSuccess(response); },
            error: function (xhr) { if(fnError != undefined) fnError(xhr); },
            complete: function (response) { if(fnComplete != undefined) fnComplete(response); }
        });
    };
    this.getList = function (last, limit, fnBeforeSend, fnSuccess, fnError, fnComplete) {
		var self = this,
            limit = limit || 20;
		$.ajax({
			url: self.path + "list/"+last+"/"+limit,
			type: "GET",
			dataType: "JSON",
            beforeSend: function () { if(fnBeforeSend != undefined) fnBeforeSend(); },
            success: function (response) { if(fnBeforeSend != undefined) fnSuccess(response); },
            error: function (xhr) { if(fnError != undefined) fnError(xhr); },
            complete: function (response) { if(fnComplete != undefined) fnComplete(response); }
		});
	};

	this.getTemplateProduct = function () {
	    return "<div class='col s12 m4 l3'><div class='card'><div class='card-image'><img src='http://lorempixel.com/200/200/food/' alt='"+this.name+"'></div><div class='card-content'><div class='product-title'><p class='product-name' id='productName"+this.id+"'>"+this.name+"</p></div><div class='product-content'><p class='product-price' id='productPrice"+this.id+"'>$ "+this.price+"</p><hr><div class='product-description'><p title='"+this.description+"'>"+this.description+"</p></div></div></div><div class='card-action'><div class='row center-align'><div class='quantityOrder' id='quantityOrder"+this.id+"'></div><button class='btn blue darken-2 agregarCarrito' id='btnAgregar"+this.id+"'><i class='material-icons'>add_shopping_cart</i></button></div></div></div></div>";
    };
}

function ILista(model) {
    this.getList = function (last, limit, fnBeforeSend, fnSuccess, fnError, fnComplete) {
        var self = this,
            last = last || 0,
            limit = limit || 20;
        $.ajax({
            url: model.path + "list/"+last+"/"+limit,
            type: "GET",
            dataType: "JSON",
            beforeSend: function () { if(fnBeforeSend != undefined) fnBeforeSend(); },
            success: function (response) { if(fnBeforeSend != undefined) fnSuccess(response); },
            error: function (xhr) { if(fnError != undefined) fnError(xhr); },
            complete: function (response) { if(fnComplete != undefined) fnComplete(response); }
        });
    };
}