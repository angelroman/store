function Item() {
    this.product_id = 0;
    this.quantity = 0;
    this.Product = {};
    this.setProduct = function (product) {
        var _product = new Product();
        this.Product = Object.assign(_product, product);
    };
    this.getTemplateModal = function () {
        return "<div class='row card' id='templateOrder-"+this.product_id+"'>" +
            "<div class='col s12 m4'>" +
            "<div class='order-product-image'>" +
            "<img src='http://lorempixel.com/400/500/nature/6'>" +
            "</div>" +
            "</div>" +
            "<div class='col s12 m8'>" +
            "<h3 class='flow-text order-product-title-modal'>"+this.Product.name+" <span class='codigo'>c&oacute;digo "+this.Product.code+"</span></h3>" +
            "<p class='order-product-price-primary' style='color: #ff8a80;'>$ "+this.Product.price+" MXN</p>" +
            "<span class='order-product-price'>$ "+this.Product.price+" MXN</span>" +
            "<div class='order-product-footer'>" +
            "<label for='order-product-quantity-"+this.product_id+"'>Cantidad: </label><input type='number' id='order-product-quantity-"+this.product_id+"' product-id='"+this.product_id+"' class='order-product-quantity quantityProduct' min='1' value='"+this.quantity+"'/>" +
            "<button class='waves-effect waves-light btn order-product-delete red' id='btnRemove-"+this.product_id+"'><i class='material-icons'>delete</i></button>" +
            "</div>" +
            "</div>" +
            "</div>";
    };
    this.getTemplate = function () {
        return "<div class='row card' id='templateOrder-"+this.product_id+"'>" +
            "<div class='col s12 m4'>" +
            "<div class='order-product-image'>" +
            "<img src='http://lorempixel.com/400/500/nature/6'>" +
            "</div>" +
            "</div>" +
            "<div class='col s12 m8'>" +
            "<h3 class='flow-text order-product-title'>"+this.Product.name+" <span class='codigo'>c&oacute;digo "+this.Product.code+"</span></h3>" +
            "<p class='order-product-price-primary' style='color: #ff8a80;'>$ "+this.Product.price+" MXN</p>" +
            "<span class='order-product-price'>$ "+this.Product.price+" MXN</span>" +
            "<div class='order-product-footer'>" +
            "<label for='order-product-quantity-"+this.product_id+"'>Cantidad: </label><input type='number' id='order-product-quantity-"+this.product_id+"' product-id='"+this.product_id+"' class='order-product-quantity quantityProduct' min='1' value='"+this.quantity+"'/>" +
            "<button class='waves-effect waves-light btn order-product-delete red' id='btnRemove-"+this.product_id+"'><i class='material-icons'>delete</i></button>" +
            "</div>" +
            "</div>" +
            "</div>";
    };


    this.AddEventChangeQuantity = function (fnOnChange) {
        var self = this;
        $('#order-product-quantity-'+this.product_id).change(function () {
            self.quantity = $(this).val();
            if(fnOnChange != undefined) { fnOnChange(); }
        });
    };
    this.AddEventRemove = function (fnOnRemove) {
        var self = this;
        $("#btnRemove-"+this.product_id).click(function () {
            $("#templateOrder-"+self.product_id).fadeOut("slow", function () {
                $(this).remove();
                if(fnOnRemove != undefined) { fnOnRemove(); }
            });
        });
    };
    this.updateTemplateQuantity = function () {
        $("#order-product-quantity-"+this.product_id).val(this.quantity);
    };
    this.getTotal = function () {
        return parseFloat(this.quantity) * parseFloat(this.Product.price);
    };
}