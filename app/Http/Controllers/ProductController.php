<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\App;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        $products = Product::get()->where("id", "<", 10);
        return view('product.index', ["products" => $products]);
    }
    public function get($id) {
        $product = Product::where('id', $id)->get()->first();
        return $product->toJson();
    }
    public function details($code) {
        $product = Product::get()->where('code', $code)->first();
        if($product) {
            return view('product.details', ["product" => $product]);
        }
        else {
            return view('layouts.not-found');
        }
    }

    public function lists() {
        $products = Product::get()->where("id", "<", 10);
        return view('product.lists', ["products" => $products]);
    }
}