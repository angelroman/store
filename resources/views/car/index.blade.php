@extends('layouts.home')
@section('title', 'Mi carrito')

@section('sectionTitle')
    Mi carrito <i class="material-icons">shopping_cart</i>
@endsection
@section('content')
    <div class="carrito-container">
        <div class="row">
            <div class="col s12 m8 order-products-list">
            </div>
            <div class="col s12 m1"></div>
            <div class="col s12 m3">
                <table id="resumenOrden">
                    <thead>
                    <tr><td colspan="2"><h4 class="flow-text">Resumen de orden</h4></td></tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Subtotal</td><td id="tdSubtotal"></td>
                    </tr>
                    <tr>
                        <td>Envio</td><td id="tdEnvio"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><div class="divider"></div></td>
                    </tr>
                    <tr>
                        <td>Total</td><td id="tdTotal"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><button class="waves-effect waves-light btn blue darken-4">Pasar a pagar</button></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/model/Product.js') }}"></script>
    <script src="{{ asset('js/model/Car.js') }}"></script>
    <script src="{{ asset('js/model/Order.js') }}"></script>
    <script src="{{ asset('js/model/Item.js') }}"></script>
    <script src="{{ asset('js/main-car.js') }}"></script>
@endsection