<!-- Load the required components. -->
<script src="https://js.braintreegateway.com/web/3.12.0/js/client.min.js"></script>
<script src="https://js.braintreegateway.com/web/3.12.0/js/paypal.min.js"></script>

<!-- Use the components. We'll see usage instructions next. -->
<script>
    // Fetch the button you are using to initiate the PayPal flow
    var paypalButton = document.getElementById('id-for-your-paypal-button');
    // Create a Client component
    braintree.client.create({
        authorization: 'TOKEN'
    }, function (clientErr, clientInstance) {
        // Create PayPal component
        braintree.paypal.create({
            client: clientInstance
        }, function (err, paypalInstance) {
            paypalButton.addEventListener('click', function () {
                // Tokenize here!
                paypalInstance.tokenize({
                    flow: 'checkout', // Required
                    amount: 10.00, // Required
                    currency: 'USD', // Required
                    locale: 'en_US',
                    enableShippingAddress: true,
                    shippingAddressEditable: false,
                    shippingAddressOverride: {
                        recipientName: 'Scruff McGruff',
                        line1: '1234 Main St.',
                        line2: 'Unit 1',
                        city: 'Chicago',
                        countryCode: 'US',
                        postalCode: '60652',
                        state: 'IL',
                        phone: '123.456.7890'
                    }
                }, function (err, tokenizationPayload) {
                    // Tokenization complete
                    // Send tokenizationPayload.nonce to server
                });
            });
        });
    });
</script>
<script src="https://www.paypalobjects.com/api/button.js?"
        data-merchant="braintree"
        data-id="paypal-button"
        data-button="checkout"
        data-color="gold"
        data-size="medium"
        data-shape="pill"
        data-button_type="submit"
        data-button_disabled="false"
></script>
<!-- Configuration options:
  data-color: "blue", "gold", "silver"
  data-size: "tiny", "small", "medium"
  data-shape: "pill", "rect"
  data-button_disabled: "false", "true"
  data-button_type: "submit", "button"
--->