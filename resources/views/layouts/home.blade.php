<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} - @yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('lib/fonts/material-design-icons/iconfont/material-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/css/materialize.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    @yield('styles')
<!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div class="backgroundSpinner">
</div>

<nav class="blue darken-2">
    {{--<div class="navbar-fixed container">
        <a id="logo-container" href="{{ url('/') }}" class="brand-logo center">Logo</a>
        <ul class="right hide-on-med-and-down">
            @if (Auth::guest())
                <li><a href="{{ route('login') }}">Iniciar sesi&oacute;n</a></li>
                <li><a href="{{ route('register') }}">Registrar</a></li>
            @else
                <li>
                    <a class="dropdown-button" href="#!"
                       data-activates="dropdown1">Hola, {{ Auth::user()->name }}
                        <i class="material-icons right">arrow_drop_down</i>
                    </a>
                </li>
            @endif
        </ul>
        <ul id="dropdown1" class="dropdown-content">
            <li>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Salir
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
        @if (Auth::guest())
        @else
            <ul id="nav-mobile" class="side-nav">
                <li><a href="/lista-productos">Productos</a></li>
                <li><a href="/categorias">Categorias</a></li>
                <li><a href="/marcas">Marcas</a></li>
                <li><a href="!#">Configuraci&oacute;n</a></li>
            </ul>
            <a href="#" style="display: block;" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        @endif
    </div>--}}
    <div class="navbar">
        <a id="logo-container" href="{{ url('/') }}" class="brand-logo center">Logo</a>
        <ul id="slide-out" class="side-nav">
            <li>
                <div class="userView">
                    <div class="background">
                        {{--<img src="images/office.jpg">--}}
                    </div>
                    <a href="#!user" class="circle" style="text-align: center; border: 1px solid #eeeeee;"><i class="black-text material-icons" style="font-size: 40px;">person</i></a>
                    @if (Auth::guest())
                        <a href="{{ route('login') }}"><span class="black-text">Iniciar sesi&oacute;n</span></a>
                        <a href="{{ route('register') }}"><span class="black-text">Registrar</span></a>
                    @else
                        <a href="#!">
                            <span class="black-text name">{{ Auth::user()->name }}</span>
                        </a>
                        <a href="{{ route('logout') }}" class="waves-effect"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <span class="black-text">Salir</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endif
                    {{--<a href="#!name"><span class="black-text name">John Doe</span></a>
                    <a href="#!email"><span class="black-text email">jdandturk@gmail.com</span></a>--}}
                </div>
            </li>
            <li><div class="divider"></div></li>
            <li>
                <a href="/car">
                    <i class="material-icons">shopping_cart</i> Mi carrito
                </a>
            </li>
            <li><a href="/products">Productos</a></li>
            <li><div class="divider"></div></li>
            <li><a class="subheader">Subheader</a></li>
            <li><a class="waves-effect" href="#!">Third Link With Waves</a></li>
            <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <a class="collapsible-header">Dropdown<i class="material-icons">arrow_drop_down</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="#!">First</a></li>
                                <li><a href="#!">Second</a></li>
                                <li><a href="#!">Third</a></li>
                                <li><a href="#!">Fourth</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse show-on-large"><i class="material-icons">menu</i></a>
    </div>
</nav>


<div id="app" class="row">
    <div class="col s12 m1"></div>
    <div class="col s12 m10">
        <div class="row">
            <h3 class="flow-text">@yield('sectionTitle')</h3>
            <div class="divider"></div>
        </div>
        @yield('content')
    </div>
    <div class="col s12 m1"></div>
</div>

<footer class="page-footer blue darken-2 myFooter">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Company Bio</h5>
                <p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>


            </div>
            <div class="col l3 s12">
                <h5 class="white-text">Settings</h5>
                <ul>
                    <li><a class="white-text" href="#!">Link 1</a></li>
                    <li><a class="white-text" href="#!">Link 2</a></li>
                    <li><a class="white-text" href="#!">Link 3</a></li>
                    <li><a class="white-text" href="#!">Link 4</a></li>
                </ul>
            </div>
            <div class="col l3 s12">
                <h5 class="white-text">Connect</h5>
                <ul>
                    <li><a class="white-text" href="#!">Link 1</a></li>
                    <li><a class="white-text" href="#!">Link 2</a></li>
                    <li><a class="white-text" href="#!">Link 3</a></li>
                    <li><a class="white-text" href="#!">Link 4</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Made by <a class="brown-text text-lighten-3" href="http://materializecss.com">Materialize</a>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script src="{{ asset('lib/js/jquery.min.js') }}"></script>
<script src="{{ asset('lib/js/materialize.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/menu.js') }}"></script>
@yield('scripts')
</body>
</html>
