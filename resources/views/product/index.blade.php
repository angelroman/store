@extends('layouts.home')
@section('title', 'Productos')
@section('sectionTitle', 'Productos')
@section('content')
    <div class="row" id="divListProducts"></div>
    <div class="row"></div>

    <div class="row load-more">
        <div class="col s12 center-align grey lighten-3">
            <i class="medium material-icons">keyboard_arrow_down</i>
        </div>
    </div>

    <div class="fixed-action-btn">
        <button class="btn-floating btn-large blue darken-2 waves-effect waves-light" id="btnCarrito">
            <i class="material-icons">shopping_cart</i>
        </button>
    </div>

    <div id="carrito" class="modal bottom-sheet">
        <div class="modal-content">
            <h4>Mi carrito <i class="material-icons">shopping_cart</i></h4>
            {{--<ul class="collection" id="productosPedidos">
            </ul>--}}

            <div class="carrito-container">
                <div class="row">
                    <div class="col s12 order-products-list">
                    </div>
                </div>
            </div>


        </div>
        <div class="modal-footer">
            <a href="car" class="btn btn-flat blue white-text" {{--id="btnCarritoComprar"--}}>Comprar</a>
            <button class="modal-action modal-close waves-effect waves-green btn-flat">Cerrar</button>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/model/Product.js') }}"></script>
    <script src="{{ asset('js/model/Order.js') }}"></script>
    <script src="{{ asset('js/model/Item.js') }}"></script>
    <script src="{{ asset('js/model/Car.js') }}"></script>
    <script src="{{ asset('js/main-products.js') }}"></script>
@endsection