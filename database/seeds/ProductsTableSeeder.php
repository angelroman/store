<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Brand;
use App\Product;
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $categories = Category::pluck('id');
        //$units = Unit::pluck('id');
        $brands = Brand::pluck('id');


        for ($i = 0; $i < 500; $i++) {
            Product::create([
                'name' => $faker->streetName().' '.$faker->randomNumber($nbDigits = NULL),
                'code' => $faker->randomNumber($nbDigits = 6),
                'price' => $faker->randomFloat($nbDecimals = 2, $min = 50, $max = 99999),
                /*'photoUrl' => $faker->imageUrl($width = 400, $height = 200, 'food'),*/
                'stok' => $faker->numberBetween($min = 0, $max = 300),
                'description' => $faker->sentence($nbWords = 25, $variableNbWords = true),
                'category_id' => $faker->randomElement($array = $categories->toArray()),
                'brand_id' => $faker->randomElement($array = $brands->toArray())
            ]);
        }
    }
}
