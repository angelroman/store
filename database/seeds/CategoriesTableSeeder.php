<?php

use Illuminate\Database\Seeder;
use App\Category;
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 20; $i++) {
            Category::create([
                'name' => $faker->lexify('Categoria ????? de ????'),
                'description' => $faker->sentence($nbWords = 20, $variableNbWords = true)
            ]);
        }
    }
}
