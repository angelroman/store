<?php

use Illuminate\Database\Seeder;
use App\Brand;
use Faker\Factory as Faker;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 50; $i++) {
            Brand::create([
                'name' => $faker->company().' '.$faker->randomNumber($nbDigits = NULL),
                'description' => $faker->sentence($nbWords = 10, $variableNbWords = true)
            ]);
        }
    }
}
